﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    public static LevelGenerator sharedInstance; //Instancia compartida para solo tener un generador de niveles
    public List<LevelBlock> allTheLevelBlocks = new List<LevelBlock>(); //Lista que contiene todos los niveles que se han creado
    public List<LevelBlock> currentLevelBLocks = new List<LevelBlock>(); //Lista de los bloques que tenemos ahora mismo en pantalla
    public Transform levelInitialPoint; //Punto inicial donde empezará a crearse el primer nivel de todos

    private bool isGeneratingInitialBlocks = false;

    void Awake()
    {
        sharedInstance = this;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        GenerateInitialBlocks();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void GenerateInitialBlocks()
    {
        isGeneratingInitialBlocks = true;
        for (int i = 0; i<3; i++)
        {
            AddNewBlock();
        }
        isGeneratingInitialBlocks = false;
    }


    //creación de bloques
    public void AddNewBlock()
    {
        //seleccionar un bloque aleatorio entre los que tenemos disponibles
        int randomIndex = Random.Range(0, allTheLevelBlocks.Count);

        if (isGeneratingInitialBlocks)
        {
            randomIndex = 0;
        }

        //crea una copia de uno de los prefabs y lo metemos en block
        LevelBlock block = (LevelBlock)Instantiate (allTheLevelBlocks[randomIndex]);

        block.transform.SetParent(this.transform, false);

        //Posicion del bloque
        Vector3 blockPosition = Vector3.zero;

        if (currentLevelBLocks.Count == 0)
        {
            //Vamos a colocar el primer bloque en pantalla
            blockPosition = levelInitialPoint.position;
        }
        else
        {
            //Colocamos el bloque en el punto de salida del anterior
            blockPosition = currentLevelBLocks[currentLevelBLocks.Count - 1].exitPoint.position;
        }

        block.transform.position = blockPosition;
        //añadimos el bloque a pantalla
        currentLevelBLocks.Add(block);

    }


    public void RemoveOldBlock()
    {
        //buscamos el bloque mas antiguo y lo eliminamos
        LevelBlock block = currentLevelBLocks[0];
        currentLevelBLocks.Remove(block);
        Destroy(block.gameObject);
        
    }

    public void RemoveAllTheBlocks()
    {
        while (currentLevelBLocks.Count > 0)
        {
            RemoveOldBlock();
        }
    }

}
